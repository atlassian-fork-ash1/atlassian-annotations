package com.atlassian.annotations;

import com.google.common.base.Function;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Sets;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

public class AnalysisResult {
    private final String clazz;
    private boolean unloadable = false;

    private Map<Method, Set<Class<?>>> nonAnnotatedMethodReferences = new MapMaker().makeComputingMap(new Function<Method, Set<Class<?>>>() {
        public Set<Class<?>> apply(final Method input) {
            return Sets.newHashSet();
        }
    });

    public AnalysisResult(final String clazz) {
        this.clazz = clazz;
    }

    public String getClassName() {
        return clazz;
    }

    public boolean isUnloadable() {
        return unloadable;
    }

    public void setUnloadable() {
        this.unloadable = true;
    }

    public Map<Method, Set<Class<?>>> getNonAnnotatedMethodReferences() {
        return nonAnnotatedMethodReferences;
    }

    public void addUnannotatedMethodReference(Method method, Class<?> reference) {
        nonAnnotatedMethodReferences.get(method).add(reference);
    }
}
