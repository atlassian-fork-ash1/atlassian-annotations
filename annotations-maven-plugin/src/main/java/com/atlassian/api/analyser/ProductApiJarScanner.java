package com.atlassian.api.analyser;

import org.apache.maven.plugin.logging.Log;
import org.dom4j.Element;
import org.objectweb.asm.ClassReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 */
public class ProductApiJarScanner {
    public void scanJar(Element productElement, String fileName, InputStream is, final Log log) {

        ProductApiClassScanner classScanner = new ProductApiClassScanner(productElement);
        try {
            ZipInputStream zip = new ZipInputStream(is);
            ZipEntry entry;
            while ((entry = zip.getNextEntry()) != null) {
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith(".class")) {
                        // Between each entry, ZipInputStream behaves like an input stream just for that entry
                        ClassReader classReader = new ClassReader(zip);
                        classReader.accept(classScanner, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES | ClassReader.SKIP_CODE);
                    } else if (entry.getName().endsWith(".jar")) {
                        // Embedded jar. Visit it!
                        scanJar(productElement, fileName + ": " + entry.getName(), zip, log);
                    }
                }
            }
        } catch (IOException ioe) {
            log.error("Error reading jar file: " + fileName, ioe);
        }

    }
}
