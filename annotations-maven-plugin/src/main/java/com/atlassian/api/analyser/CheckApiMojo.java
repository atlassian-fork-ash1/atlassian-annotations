package com.atlassian.api.analyser;

import com.atlassian.api.analyser.rules.NoAddAbstractMethodRule;
import com.atlassian.api.analyser.rules.NoAddExceptionRule;
import com.atlassian.api.analyser.rules.NoChangeProtectedFieldTypeRule;
import com.atlassian.api.analyser.rules.NoChangePublicFieldTypeRule;
import com.atlassian.api.analyser.rules.NoRemoveClassRule;
import com.atlassian.api.analyser.rules.NoRemoveExceptionRule;
import com.atlassian.api.analyser.rules.NoRemoveProtectedConcreteMethodRule;
import com.atlassian.api.analyser.rules.NoRemoveProtectedFieldRule;
import com.atlassian.api.analyser.rules.NoRemovePublicFieldRule;
import com.atlassian.api.analyser.rules.NoRemovePublicMethodRule;
import com.atlassian.api.analyser.rules.Rule;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang.StringUtils.split;

/**
 * This class checks 2 (or multiple) versions of our API to make sure we have not broken the api contract.
 * <p/>
 * We compare the JAR artifact from the build with a reference baseline XML signature file
 *
 *
 * This class should be run over the definitive shipped version of the product at each release to produce a final
 * artifact for that version.
 * <p/>
 * This uses ASM to proceses the classes in the jar file.
 *
 * Exclusions can be supplied in an XML file of the form
 * <pre>
 * &lt;exclusions&gt;
 *  &lt;class name="com/atlassian/query/Query" exclude="true" /&gt;
 *  &lt;class name="com/atlassian/query/operator/Operator"&gt;
 *   &lt;field name="LIKE" exclude="true" /&gt;
 *   &lt;field name="NOT_LIKE" exclude="true" /&gt;
 *   &lt;method name="values" desc="()[Lcom/atlassian/query/operator/Operator;" exclude="true" /&gt;
 *   &lt;method name="valueOf" desc="(Ljava/lang/String;)Lcom/atlassian/query/operator/Operator;" access="PUBLIC"/&gt;
 *  &lt;/class&gt;
 * &lt;/exclusions&gt;
 * </pre>
 *
 * <p/>
 * The name of the exclusions file then needs to be passed on the exclusionsXML parameter.
 *
 * @goal check-api
 * @phase verify
 */
public class CheckApiMojo extends AbstractMojo {
    private static final List<Rule> apiRules = new ArrayList<Rule>();
    private static final List<Rule> spiRules = new ArrayList<Rule>();

    static {
        apiRules.add(new NoRemoveClassRule());
        // Check for public -> downgrade
        apiRules.add(new NoRemovePublicMethodRule());
        apiRules.add(new NoRemovePublicFieldRule());
        apiRules.add(new NoChangePublicFieldTypeRule());
        apiRules.add(new NoAddExceptionRule());
    }

    static {
        spiRules.add(new NoRemoveClassRule());
        spiRules.add(new NoRemoveProtectedConcreteMethodRule());
        spiRules.add(new NoRemoveProtectedFieldRule());
        spiRules.add(new NoChangeProtectedFieldTypeRule());
        spiRules.add(new NoAddAbstractMethodRule());
        spiRules.add(new NoRemoveExceptionRule());
    }

    /**
     * @component
     */
    protected org.apache.maven.artifact.resolver.ArtifactResolver resolver;

    /**
     * @component
     */
    protected ArtifactFactory artifactFactory;

    /**
     * Location of the local repository.
     *
     * @parameter expression="${localRepository}"
     * @readonly
     * @required
     */
    protected ArtifactRepository localRepository;

    /**
     * Baseline artifact version to check. This can be a comma-separated string of versions.
     *
     * @parameter expression="${checkApi.comparisonVersion}"
     */
    protected String comparisonVersion;

    /**
     * Baseline xml to check. This can be a comma-separated list of xml files.
     *
     * @parameter expression="${checkApi.baselineXml}"
     */
    protected String baselineXml;

    /**
     * Location of the local repository.
     *
     * @parameter expression="${checkApi.exclusionsXml}"
     */
    protected String exclusionsXml;

    /**
     * @parameter default-value="${project}"
     */
    protected org.apache.maven.project.MavenProject mavenProject;

    public void execute() throws MojoExecutionException, MojoFailureException {

        ApiCheckExecutor checker = new ApiCheckExecutor(this);
        if (comparisonVersion != null) {
            for (String version : split(comparisonVersion, ",")) {
                checker.checkBaselineArtifact(version);
            }
        }

        if (baselineXml != null) {
            for (String xml : split(baselineXml, ",")) {
                checker.checkBaselineXml(xml);
            }
        }
    }

}
