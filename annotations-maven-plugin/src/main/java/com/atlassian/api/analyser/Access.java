package com.atlassian.api.analyser;

import org.objectweb.asm.Opcodes;

public enum Access {
    PUBLIC,
    PROTECTED,
    PACKAGE,
    PRIVATE;

    public static Access fromOpcode(int opcode) {
        if ((opcode & Opcodes.ACC_PUBLIC) == Opcodes.ACC_PUBLIC) return PUBLIC;
        if ((opcode & Opcodes.ACC_PROTECTED) == Opcodes.ACC_PROTECTED) return PROTECTED;
        if ((opcode & Opcodes.ACC_PRIVATE) == Opcodes.ACC_PRIVATE) return PRIVATE;
        return PACKAGE;
    }
}
