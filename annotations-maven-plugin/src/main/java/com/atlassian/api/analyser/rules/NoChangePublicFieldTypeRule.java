package com.atlassian.api.analyser.rules;

import com.atlassian.api.analyser.Access;
import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiField;

import java.util.List;
import java.util.Map;

/**
 * You are not allowed to change the type of public fields from an api class.
 *
 * @since v5.0
 */
public class NoChangePublicFieldTypeRule implements Rule {

    public void apply(String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        // For the class in the baseline ensure all public fields are also present in the current;
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiField baselineField : baseLineClass.getFields().keySet()) {
            if (baselineField.getAccess() == Access.PUBLIC) {
                ApiField currentField = currentClass.getFields().get(baselineField);
                if (currentField == null) {
                    // This is not our problem.  Another rule will check this if needs be.
                    continue;
                }
                if (!baselineField.getDesc().equals(currentField.getDesc())) {
                    errors.add("Class - '" + className + "' : Public field '" + baselineField.getFieldName() + "' type changed.");
                }
            }
        }
    }
}
