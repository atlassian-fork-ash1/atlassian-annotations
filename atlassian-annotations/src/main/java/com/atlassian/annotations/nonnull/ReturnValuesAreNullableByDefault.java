package com.atlassian.annotations.nonnull;

import javax.annotation.Nullable;
import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Similar to {@link javax.annotation.ParametersAreNullableByDefault}, but applies to return values of methods.
 *
 * @deprecated since 2.2.0. Use corresponding Checker Framework based annotation {@link com.atlassian.annotations.nullability.ReturnValuesAreNullableByDefault}
 */
@Documented
@Nullable
@TypeQualifierDefault({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface ReturnValuesAreNullableByDefault {
}
