package com.atlassian.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark a code construct whose usage or implementation is changing as part of a staged feature rollout.
 *
 * @since v1.2.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface StagedRollout {
    /**
     * @return a string identifier for grouping the configuration into a higher level feature.
     */
    String group();

    /**
     * The name of the configuration which alters the implementation behaviour. This should be the name of a system
     * property, or a server specific name for a dark feature or other configuration mechanism.
     */
    String property();
}
