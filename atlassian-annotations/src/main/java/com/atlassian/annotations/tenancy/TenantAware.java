package com.atlassian.annotations.tenancy;

import com.atlassian.annotations.Internal;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A marker interface for indicating whether or not a component data management (particularly any caches that it
 * uses) has been evaluated for its safe use in a multi-tenanted environment.
 * <p>
 * See {@link TenancyScope} for acceptable values and their meanings.
 * </p>
 *
 * @see TenancyScope
 * @since v0.20
 */
@Internal
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface TenantAware {
    TenancyScope value();
    String comment() default "";
}