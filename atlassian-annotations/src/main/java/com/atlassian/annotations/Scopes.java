package com.atlassian.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotated element is part of one or more API "scopes".
 * <p>
 * This element is designed for Atlassian Connect add-ons to <em>request</em> scopes to gain access to a remote API.
 * <p>
 *
 * @since 0.8
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.PACKAGE, ElementType.METHOD, ElementType.TYPE})
public @interface Scopes {
    String[] value();
}
